# Wepublish helm-charts

## Helm chart repository

Repository is accessible at https://gitlab.com/api/v4/projects/38941346/packages/helm/stable, run:
```
helm repo add wepublish https://gitlab.com/api/v4/projects/38941346/packages/helm/stable
```

## Install apps

Example installation using helm cli.
```
helm install --dry-run reatch-app wepublish/wepublish
```
